var restify = require('restify');
var builder = require('botbuilder');
var Conversation = require('watson-developer-cloud/conversation/v1');

WatsonConversation = require('./src/class/conversation');
Message = require('./src/class/message');
Config = require('./src/class/config');
Labels = require('./src/services/labels');
Chatbot = require('./src/class/chatbot');
var EventEmitter = require("events").EventEmitter;

require('dotenv').config({ silent: true });

var config = new Config( process.env.FAQApi, process.env.subscriptionKeyFAQ );
var labels = new Labels(config.currentLang);
var chatbot = new Chatbot({});

var workspace = process.env.WORKSPACE_ID || '';

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log('%s listening to %s', server.name, server.url);
});

var connector = chatbot.getConnector(process.env.appId, process.env.appPassword);
server.post('/api/messages', connector.listen());

var conversation = new WatsonConversation();
conversation.createNewConversation('A', new Conversation({
    url: config.watsonGateway,
    version_date: Conversation.VERSION_DATE_2017_04_21
}));
conversation.setCurrentConversation('A');

var bot = new builder.UniversalBot(connector, function(session) {
    
    var message = new Message(session );
    message.defaultParams = message.getParamsFromUserData(session.message.user);

    var payload = {
        workspace_id: workspace,
        context: '',
        input: { text: session.message.text }
    };

    var conversationContext = chatbot.findOrCreateContext(session.message.address.conversation.id, workspace);
    if (!conversationContext) conversationContext = {};
    payload.context = conversationContext.watsonContext;

    conversation.getCurrentConversation().message(payload, function(err, response) {
        if (err) {
            message.send(err);
        } else {

            message.send(response.output.text);
            conversationContext.watsonContext = response.context;

            if (response.output && response.output.hasOwnProperty('actions')) {
                var eventEmmiter = new EventEmitter();
                var action = new Action();
                eventEmmiter.on("messageSent", function(text) {
                    message.send(text);
                });
                action.executeActionsByResponse(response, eventEmmiter, message);
            }
        }
    });

});