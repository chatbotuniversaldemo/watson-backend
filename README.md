# WatsonBackend

Integraciones entre el(los) chatbots de universal y acciones o servicios propias del negocio.
???????????????????????

## Prerequisites

Los requisitos minimos para correr este proyectos son:
* Tener acceso de forma local a la base de datos para ejecutar las acciones que lo requieran.    
* Visual Studio 2017 o superior.

## Instrucciones

* Para cambiar el connection string y asi conectarse a la base de datos que desee, debe hacerlo en el archivo appsettings.json en el key "ConnectionString":

```Json
  "ApplicationSettings": {
    "Applications": "ACSEL:ACTIVE,APOLO:ACTIVE,VMU:ACTIVE",
    "ConnectionString": "Data Source=172.16.99.111:1522/PX05; Pooling=true;User id=uniserv; password=uniserv;"
  },
```

## Correr los tests

Las pruebas unitarias pueden correr abriendo la solucion en Visual Studio 2017 y activando la opcion de "live testing" tal y como se especifica en la [documentacion oficial](https://docs.microsoft.com/en-us/visualstudio/test/live-unit-testing).