var sanitizer = require('sanitizer');

module.exports = class Message {
    constructor( session, defaultParams )
    {
        this.session = session;
        this.defaultParams = defaultParams ? defaultParams : [];
    }

    send( text )
    {
        if (text != 'NO_MESSAGE' && text != '') {
            let msj = typeof text == 'string' ? text : text[0];
            msj = this.setParamsValueToMsj(msj, this.defaultParams);
            this.session.send( sanitizer.unescapeEntities(msj) );
        }
    }

    setParamsValueToMsj(msj, params)
    {
        if(params && params.length > 0)
        {
            params.forEach((param)=>{
                msj = msj.replace('{{'+param.name+'}}', param.value );
            });
        }
        return msj;
    }

    getParamsFromUserData( userData )
    {
        let params = [];
        for (var property in userData) {
            if (userData.hasOwnProperty(property)) {
                params.push( { name: property, value: userData[property] } );
            }
        }
        return params;
    }
}