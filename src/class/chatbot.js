var builder = require('botbuilder');

module.exports = class Chatbot {
    constructor( contextsToSet )
    {
        this.contexts = contextsToSet;
    }

    getConnector(appId, appPassword) {
        return new builder.ChatConnector({
            appId: process.env.appId,
            appPassword: process.env.appPassword
        });
    }

    findOrCreateContext(convId, workspace) {
        if (!this.contexts)
            this.contexts = [];

        if (!this.contexts[convId]) {
            this.contexts[convId] = { workspaceId: workspace, watsonContext: {} };
        }
        return this.contexts[convId];
    }
}