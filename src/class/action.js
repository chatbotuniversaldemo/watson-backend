var request = require('request');

Config = require('./config');
require('dotenv').config({ silent: true });

var config = new Config( process.env.FAQApi, process.env.subscriptionKeyFAQ );

Labels = require('../services/labels');
var labels = new Labels(config.currentLang);

ActionDetail = require('../class/action-detail');

module.exports = class Action {
    executeActionsByResponse(response, eventEmmiter, message) {
        response.output.actions.forEach((action) => {
            switch (action.name) {
                case 'checkSystems':
                    var actionDetail = this.getActionDetailForCheckSystems(eventEmmiter, response.entities);
                    break;
                case 'FixCrash':
                    var actionDetail = this.getActionDetailForFixCrash(message ? message.defaultParams[0].value : '', eventEmmiter);
                    break;
                case 'OpenRequest':
                    var actionDetail = this.getActionDetailForOpenRequest(action.email, action.request.description, action.request.category, eventEmmiter);
                    break;
                case 'FAQ':
                var actionDetail = this.getActionDetailForFAQ(response.input.text, eventEmmiter);
                break;
            }

            this.makeRequest(actionDetail);
        });
    }

    makeRequest(actionDetail) {
        if(actionDetail) {
            if(actionDetail.type == 'POST') {
                let options = {
                    headers: actionDetail.headers,
                    uri: actionDetail.url,
                    body: actionDetail.request,
                    responseType: 'buffer',
                    json: true
                  };

                request.post(options,
                    actionDetail.callback
                );
            } else {
                request.get(
                    actionDetail.url,
                    actionDetail.callback
                );
            }
        }
    }

    getActionDetailForOpenRequest(userCode, description, category, eventEmmiter) {
        return new ActionDetail('request', {
            "UserCode": userCode,
            "Date": new Date(),
            "Description": description.replace('{{userCode}}', userCode),
            "Category": category
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                eventEmmiter.emit("messageSent", labels.getLabelByKey('requestOpened', [{ name: 'requestNumber', value: body.requestCode }]) );
            }
        }, 'POST');
    }

    getActionDetailForCheckSystems(eventEmmiter, entities) {
        let relevantAppsForUser = [];
        if(entities && entities.length > 0)
        {
            entities.forEach((entity)=>{
                if(entity.entity == 'system')
                {
                    relevantAppsForUser.push(entity.value.toUpperCase());
                }
            });
        }

        return new ActionDetail('application', {}, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var appsDown = [];
                JSON.parse(body).forEach((application) => {
                    if (application.status == 'INACTIVE') {
                        appsDown.push(application.name);
                    }
                });
                let appsUp = [];
                if(relevantAppsForUser.length > 0)
                {                    
                    JSON.parse(body).forEach((application) => {                        
                        relevantAppsForUser.forEach((appName)=>{
                            if(application.status == 'ACTIVE' && appName == application.name.toUpperCase())
                            {
                                appsUp.push(appName);
                            }
                        });
                    });                    
                }
                if (appsUp.length > 0) {
                    eventEmmiter.emit("messageSent", labels.getLabelByKey('applicationsUpMsj', [{ name: 'applications', value: appsUp.join(', ') }]));
                } else if (appsDown.length > 0) {
                    eventEmmiter.emit("messageSent", labels.getLabelByKey('applicationsDownMsj', [{ name: 'applications', value: appsDown.join(', ') }]));
                }
            }
        }, 'GET');
    }

    getActionDetailForFixCrash(email, eventEmmiter) {
        return new ActionDetail('fix', {
            "email": email
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                eventEmmiter.emit("messageSent", labels.getLabelByKey('readyMsj'));
            } else {
                if (JSON.parse(body).status == 'user_not_found') {
                    eventEmmiter.emit("messageSent", labels.getLabelByKey('noSessionOpened'));
                } else {
                    eventEmmiter.emit("messageSent", labels.getLabelByKey('unexpectedError'));
                }
            }
        }, 'POST');
    }

    getActionDetailForFAQ(question, eventEmmiter)
    {
        return new ActionDetail('faq', {
            "question": question
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                if(body.answers.length > 0 && body.answers[0].score > 80)
                {
                    eventEmmiter.emit("messageSent", body.answers[0].answer);
                } else {
                    eventEmmiter.emit("messageSent", labels.getLabelByKey('noUnderstand'));
                }
            } else {
                eventEmmiter.emit("messageSent", labels.getLabelByKey('noUnderstand'));
            }
        }, 'POST', 'FAQ');
    }
}