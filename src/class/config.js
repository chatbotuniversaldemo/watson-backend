var jsonObj = require('config.json')('config.json');

module.exports = class Config {
    constructor( FAQApi, subscriptionKeyFAQ ) {
        this.apiMock = jsonObj.apiMock;
        this.FAQApi = FAQApi ? FAQApi : jsonObj.FAQApi;
        this.currentLang = jsonObj.currentLang;
        this.subscriptionKeyFAQ = subscriptionKeyFAQ ? subscriptionKeyFAQ : jsonObj.subscriptionKeyFAQ;
    }

    getApimockUrlWithMethodName(methodName) {
        return this.apiMock + '/' + methodName;
    }
}