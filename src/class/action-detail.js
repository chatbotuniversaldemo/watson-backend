Config = require('../class/config');
require('dotenv').config({ silent: true });
var config = new Config(process.env.FAQApi, process.env.subscriptionKeyFAQ);

module.exports = class ActionDetail {
    constructor(name, request, callback, type, api) {
        this.name = name;
        this.request = request;
        this.callback = callback;
        if( !type )
        {
            this.setPost();
        } else {
            this.type = type;
        }

        this.api = api != null ? api : 'UNIVERSAL';
    }

    get headers()
    {
        return this.isUniversal() ? {} : {'Ocp-Apim-Subscription-Key': config.subscriptionKeyFAQ};
    }

    get url() {
        return this.isUniversal() ? config.getApimockUrlWithMethodName(this.name) : config.FAQApi;
    }

    isUniversal()
    {
        return this.api == 'UNIVERSAL';
    }

    setGet()
    {
        this.type = "GET";
    }

    setPost()
    {
        this.type = "POST";
    }
}