var EventEmitter = require("events").EventEmitter;
Action = require('./action');

module.exports = class WatsonConversation {
    constructor() {
        this.conversations = {};
    }

    createNewConversation(key, conversation) {
        this.conversations[key] = conversation;
    }

    setCurrentConversation(currentKey) {
        this.currentKey = currentKey;
    }

    getCurrentConversation() {
        return this.conversations[this.currentKey];
    }

    checkForNewMessage(payload, message) {
        this.getCurrentConversation().message(payload, function(err, response) {
            if (err) {
                message.send(err);
            } else {

                message.send(response.output.text);
                payload.context.watsonContext = response.context;

                if (response.output.hasOwnProperty('actions')) {
                    var eventEmmiter = new EventEmitter();
                    var action = new Action();
                    eventEmmiter.on("messageSent", function(text) {
                        message.send(text);
                    });
                    action.executeActionsByResponse(response, eventEmmiter);
                }
            }
        });
    }
}