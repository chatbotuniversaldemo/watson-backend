var fakeBd = require('config.json')('config.json');

module.exports = class Labels {
    constructor(langToSet) {
        fakeBd.langs.forEach((lang) => {
            if (lang.name == langToSet) {
                this.currentLang = lang;
            }
        });
    }

    getLabelByKey(key, params = []) {
        let labelToReturn = '';
        this.currentLang.labels.forEach((label) => {
            if (label.key == key) {
                labelToReturn = label.value;
                params.forEach((param) => {
                    labelToReturn = labelToReturn.replace('{{' + param.name + '}}', param.value);
                });
            }
        });
        return labelToReturn;
    }
}