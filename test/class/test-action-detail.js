var chai = require('chai');
var expect = chai.expect;
var ActionDetail = require('../../src/class/action-detail');
Config = require('../../src/class/config');
require('dotenv').config({ silent: true });

var config = new Config( process.env.FAQApi, process.env.subscriptionKeyFAQ );

describe('ActionDetail', function() {
  var action = new ActionDetail( '', {}, function(){} );

  it('isUniversal return true when api is UNIVERSAL',function(){
    expect(true).to.equal(action.isUniversal());
  });

  it('isUniversal return false when api is FAQ',function(){
    var action2 = new ActionDetail( '', {}, function(){}, 'GET', 'FAQ' );

    expect(false).to.equal(action2.isUniversal());
  });

  it('headers is not empty object when api is FAQ',function(){
    var action2 = new ActionDetail( '', {}, function(){}, 'GET', 'FAQ' );

    expect(1).to.equal(Object.keys(action2.headers).length);
  });

  it('headers is empty object by default',function(){
    expect(0).to.equal(Object.keys(action.headers).length);
  });

  it('api has FAQ when it is passed on constructor',function(){
    var action2 = new ActionDetail( '', {}, function(){}, 'GET', 'FAQ' );

    expect('FAQ').to.equal(action2.api);
  });

  it('api has UNIVERSAL value by default',function(){
    action.setPost();

    expect('UNIVERSAL').to.equal(action.api);
  });

  it('action set POST type by default',function(){
    action.setPost();

    expect('POST').to.equal(action.type);
  });

  it('action constructor set type',function(){
    var action = new ActionDetail( '', {}, function(){}, 'GET' );

    expect('GET').to.equal(action.type);
  });

  it('setGet set type GET',function(){
      action.setGet();

      expect('GET').to.equal(action.type);
  });

  it('setPost set type POST',function(){
      action.setPost();

      expect('POST').to.equal(action.type);
  });

  it('url property', function() {
    var name = 'FixCrash';
    var actionDetail = new ActionDetail( 'FixCrash', {}, function(){} );

    expect(actionDetail.url).to.equal(config.apiMock + '/' + name  );
  });

  it('url property return FAQApi url when api is FAQ', function() {
    var name = 'FixCrash';
    var actionDetail = new ActionDetail( 'FixCrash', {}, function(){}, 'GET', 'FAQ' );

    expect(actionDetail.url).to.equal(config.FAQApi  );
  });

  it('name property is set in constructor', function() {
    var name = 'FixCrash';

    var actionDetail = new ActionDetail( 'FixCrash', {}, function(){} );

    expect(actionDetail.name).to.equal( name  );
  });

  it('request property is set in constructor', function() {
    var request = { yolo: "You only" };

    var actionDetail = new ActionDetail( '', request, function(){} );

    expect(actionDetail.request.yolo).to.equal( request.yolo );
  });

  it('callback property is set in constructor', function() {
    var callback = function() { return 'Yes, I am here!!'; };

    var actionDetail = new ActionDetail( '', {}, callback );

    expect(actionDetail.callback()).to.equal( 'Yes, I am here!!' );
  });
});