var chai = require('chai');
var expect = chai.expect;
Chatbot = require('../../src/class/chatbot');

describe('Chatbot', function () {
    let chatbot = new Chatbot({id: '21356879'});

    it('getConnector return ChatConnector',function(){
        let connector = chatbot.getConnector( '1234', '1234' );

        expect(typeof connector).to.equal('object');
    });

    it('constructor set contexts', function(){
        expect(chatbot.contexts.id).to.equal('21356879');
    });

    it('findOrCreateContext set contexts egual to empty array when it is null', function(){
        chatbot.contexts = null;

        chatbot.findOrCreateContext( 1, 2 );

        expect(chatbot.contexts.length).to.equal(2);
    });

    it('findOrCreateContext set contexts egual to empty array when it is null', function(){
        chatbot.contexts = null;

        chatbot.findOrCreateContext( 'A', 222 );

        expect(chatbot.contexts['A']).to.not.equal(null);
    });

    it('findOrCreateContext set contexts egual to empty array when it is null', function(){
        chatbot.contexts = null;

        chatbot.findOrCreateContext( 'A', 222 );

        expect(chatbot.contexts['A'].workspaceId).to.equal(222);
    });
});