var chai = require('chai');
var expect = chai.expect;
Labels = require('../../src/services/labels');
var labels = new Labels('es');

describe('Labels', function () {
    it('contructor like currentLang the correct', function () {
        expect(labels.currentLang.name).to.equal('es');
    });

    it('getLabelByKey return correct label text by their key', function () {
        expect(labels.getLabelByKey('noSessionOpened')).to.equal('Este usuario no tiene ninguna sesion abierta.');
    });

    it('getLabelByKey return correct label text whith param', function () {
        expect(labels.getLabelByKey('requestOpened', [ { name: 'requestNumber', value: 'R-15' } ] )).to.equal('He abierto una solicitud para esos fines, el numero es: R-15');
    });
});