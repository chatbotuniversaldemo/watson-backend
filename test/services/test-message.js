var chai = require('chai');
var expect = chai.expect;
var spies = require('chai-spies');
Message = require('../../src/class/message');

chai.use(spies);
const sandbox = chai.spy.sandbox();

describe('Message', function () {
    let fakeSession = { send(){} };
    let message = new Message(fakeSession, { name: 'hello', value: 'world' });

    afterEach(() => {
        sandbox.restore();
    });

    it('constructor set session', function () {
        expect( message.session ).to.not.equal( null );
    });

    it('constructor set defaultParams', function () {
        expect( message.defaultParams ).to.not.equal( null );
    });

    it('send call send on session', function () {
        sandbox.on(message.session, ['send']);

        message.send('Hello');

        expect(message.session.send).to.have.been.called();
    });

    it('send not call send on session when it is set and message es NO_MESSAGE', function () {
        sandbox.on(message.session, ['send']);

        message.send('NO_MESSAGE');

        expect(message.session.send).to.not.have.been.called();
    });

    it('setParamsValueToMsj is called when send is called', function () {
        sandbox.on(message, ['setParamsValueToMsj']);

        message.send('my msj');

        expect(message.setParamsValueToMsj).to.have.been.called();
    });

    it('setParamsValueToMsj return same msj if params is null',function(){
        expect(message.setParamsValueToMsj('hello {{myParam}}', null)).to.equal('hello {{myParam}}');
    });

    it('setParamsValueToMsj return same msj if params is empty array',function(){
        expect(message.setParamsValueToMsj('hello {{myParam}}', [])).to.equal('hello {{myParam}}');
    });

    it('setParamsValueToMsj return msj with param replaced with value',function(){
        expect(message.setParamsValueToMsj('hello {{myParam}}', [ {name:'myParam', value:'world'} ])).to.equal('hello world');
    });

    it('getParamsFromUserData return params with user data', function(){
        let userData = {name: 'Miguel'};

        let params = message.getParamsFromUserData(userData);

        expect(params[0].name).to.equal('name');
        expect(params[0].value).to.equal('Miguel');
    });
});