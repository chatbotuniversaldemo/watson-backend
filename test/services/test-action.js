var chai = require('chai');
var spies = require('chai-spies');
var expect = chai.expect;
var EventEmitter = require("events").EventEmitter;
Action = require('../../src/class/action');

chai.use(spies);
const sandbox = chai.spy.sandbox();

describe('Action', function () {
    let action = new Action();
    let eventEmmiter = new EventEmitter();
    let fakeResponse = { output: { actions: [ { name: '' } ] }, input: { text: 'Hello world' } };

    afterEach(() => {
        sandbox.restore();
    });

    it('getActionDetailForFAQ return action with correct name', function () {
        expect(action.getActionDetailForFAQ('hello', eventEmmiter).name).to.equal('faq');
    });

    it('getActionDetailForFixCrash return action with correct name', function () {
        expect(action.getActionDetailForFixCrash('', eventEmmiter).name).to.equal('fix');
    });

    it('getActionDetailForCheckSystems return action with correct name', function () {
        expect(action.getActionDetailForCheckSystems(eventEmmiter).name).to.equal('application');
    });

    it('getActionDetailForOpenRequest return action with correct name', function () {
        expect(action.getActionDetailForOpenRequest('', '', '', eventEmmiter).name).to.equal('request');
    });

    it('getActionDetailForFAQ return action with correct question attr on request object', function () {
        let question = 'what?';

        expect(action.getActionDetailForFAQ(question, eventEmmiter).request.question).to.equal(question);
    });

    it('getActionDetailForFixCrash return action with correct userCode attr on request object', function () {
        let email = 'mrivera@universal.com.do';

        expect(action.getActionDetailForFixCrash(email, eventEmmiter).request.email).to.equal(email);
    });

    it('getActionDetailForCheckSystems return action with empty object', function () {

        expect(Object.keys(action.getActionDetailForCheckSystems(eventEmmiter).request).length).to.equal(0);
    });

    it('getActionDetailForOpenRequest return action with correct userCode attr on request object', function () {
        let userCode = 'U29586';

        expect(action.getActionDetailForOpenRequest(userCode, '', '', eventEmmiter).request.UserCode).to.equal(userCode);
    });

    it('getActionDetailForOpenRequest return action with correct date attr on request object', function () {
        let date = new Date();

        expect(action.getActionDetailForOpenRequest('', '', '', eventEmmiter).request.Date.getTime()).to.equal(date.getTime());
    });

    it('getActionDetailForOpenRequest return action with correct description attr on request object', function () {
        let description = 'That request is so great';

        expect(action.getActionDetailForOpenRequest('', description, '', eventEmmiter).request.Description).to.equal(description);
    });

    it('getActionDetailForOpenRequest return action with correct description when has userCode interpolation', function () {
        let description = 'That request is so great for {{userCode}}';

        expect(action.getActionDetailForOpenRequest('U29586', description, '', eventEmmiter).request.Description).to.equal('That request is so great for U29586');
    });

    it('getActionDetailForOpenRequest return action with correct category attr on request object', function () {
        let category = 'install_application';

        expect(action.getActionDetailForOpenRequest('', '', category, eventEmmiter).request.Category).to.equal(category);
    });

    it( 'executeActionsByResponse getActionDetailForCheckSystems is called when action with checkSystems exists', function() {
        sandbox.on(action, ['getActionDetailForCheckSystems']);
        fakeResponse.output.actions[0].name = 'checkSystems';

        action.executeActionsByResponse( fakeResponse, eventEmmiter );

        expect(action.getActionDetailForCheckSystems).to.have.been.called();
    });

    it( 'executeActionsByResponse getActionDetailForFixCrash is called when action with FixCrash exists', function() {
        sandbox.on(action, ['getActionDetailForFixCrash']);
        fakeResponse.output.actions[0].name = 'FixCrash';

        action.executeActionsByResponse( fakeResponse, eventEmmiter );

        expect(action.getActionDetailForFixCrash).to.have.been.called();
    });

    it( 'executeActionsByResponse getActionDetailForOpenRequest is called when action with OpenRequest exists', function() {
        sandbox.on(action, ['getActionDetailForOpenRequest']);
        fakeResponse.output.actions[0].name = 'OpenRequest';
        fakeResponse.output.actions[0].request = { description: '', category: '' };

        action.executeActionsByResponse( fakeResponse, eventEmmiter );

        expect(action.getActionDetailForOpenRequest).to.have.been.called();
    });

    it( 'executeActionsByResponse getActionDetailForFAQ is called when action with FAQ exists', function() {
        sandbox.on(action, ['getActionDetailForFAQ']);
        fakeResponse.output.actions[0].name = 'FAQ';

        action.executeActionsByResponse( fakeResponse, eventEmmiter );

        expect(action.getActionDetailForFAQ).to.have.been.called();
    });
});