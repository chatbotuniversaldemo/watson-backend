var chai = require('chai');
var expect = chai.expect;
WatsonConversation = require('../../src/class/conversation');
Action = require('../../src/class/action');

describe('WatsonConversation', function () {
    let fakeConversationInstance = { id: '123456789' };
    let conversation = new WatsonConversation( {id: '1234579d'} );

    it('conversations has empty object', function() {
        expect( Object.keys(conversation.conversations).length ).to.equal(0);
    });

    it('createNewConversation set conversation by key', function() {
        conversation.createNewConversation( 'A', fakeConversationInstance );

        expect( conversation.conversations['A'].id ).to.equal('123456789');
    });

    it('setCurrentConversation set currentKey', function() {
        conversation.setCurrentConversation( 'A' );

        expect( conversation.currentKey ).to.equal('A');
    });

    it('getCurrentConversation get current conversation by current key', function() {
        expect( conversation.getCurrentConversation().id ).to.equal('123456789');
    });
});