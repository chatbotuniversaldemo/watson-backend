var chai = require('chai');
var expect = chai.expect;
Config = require('../../src/class/config');
require('dotenv').config({ silent: true });

var config = new Config( process.env.FAQApi, process.env.subscriptionKeyFAQ );
var jsonObj = require('config.json')('config.json');

describe('Config', function () {
    it('getApimockUrlWithMethodName return url with method name', function () {
        expect(config.getApimockUrlWithMethodName('FixCrash')).to.equal(jsonObj.apiMock + '/FixCrash');
    });

    it('apiMock attr exists', function () {
        expect(config.apiMock).to.equal(jsonObj.apiMock);
    });

    it('currentLang attr exists', function () {
        expect(config.currentLang).to.equal(jsonObj.currentLang);
    });

    it('FAQApi attr exists', function () {
        expect(config.FAQApi).to.equal(jsonObj.FAQApi);
    });

    it('subscriptionKeyFAQ attr exists', function () {
        expect(config.subscriptionKeyFAQ).to.equal(jsonObj.subscriptionKeyFAQ);
    });
});